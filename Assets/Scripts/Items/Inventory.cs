﻿//actual invetory where player can access things

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
	public int _slotsX, _slotsY;
	public List<Item> _slots = new List<Item> ();
	public  GUISkin	_skin;
	public List <Item> _inventory = new List<Item>();
	private ItemDataBase _database;

	public bool _showInventory;
	private bool _showToolTip;
	private string _toolTipInfo;

	public void Start()
	{
		for (int it = 0; it < (_slotsX * _slotsY); it++) 
		{			
			_slots.Add (new Item ());
			_inventory.Add (new Item ());	
			
		}

		_database = GameObject.FindGameObjectWithTag ("Item Database").GetComponent<ItemDataBase> ();
		AddItem (0);
		AddItem (1);

		//print will return true or false if item id of 0 is in the inventory
		//print (InventoryContains(0));

//		_inventory.Add (_database.items [0]);
//		_inventory.Add (_database.items [1]);
	}
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.I)) 
		{
			_showInventory = !_showInventory;
			if (_showInventory == false) 
			{
				_showToolTip = false;
			}
			else if (_showInventory == true) 
			{
			}
		}

	}

	public void DisplayInventory()
	{
		_showInventory = !_showInventory;
		if (_showInventory == false) 
		{
			_showToolTip = false;
		}
		else if (_showInventory == true) 
		{
		}
	}

	void OnGUI()
	{
		_toolTipInfo = "";
		GUI.skin = _skin;
		if (_showInventory == true) 
		{
			DrawInventory ();
		
		}

		if (_showToolTip == true) 
		{
			GUI.Box (new Rect (Event.current.mousePosition.x + 15f,Event.current.mousePosition.y,200,200), _toolTipInfo, _skin.GetStyle("Tooltip"));
		}
		//prints names
//		for (int it = 0; it < _inventory.Count	; it++) 
//		{
//			GUI.Label (new Rect(10,it * 20,200,50),_inventory[it]._strItemName);
//		}
	}

	void DrawInventory()
	{
		Event myEvent = Event.current;
		int indexValue = 0;

		for (int y = 0; y < _slotsY; y++) 
		{
			for (int x = 0; x < _slotsX; x++) 
			{
				Rect slotRect = new Rect (x*60,y*60,50,50);
				GUI.Box(slotRect, "",_skin.GetStyle("Slots"));
				_slots [indexValue] = _inventory [indexValue];
				Item item = _slots [indexValue];

				//if there is something in the inventory that isnt a blank, draw texture and be clickable
				if(_slots[indexValue]._strItemName != null)
				{
				
					GUI.DrawTexture (slotRect, _slots [indexValue]._itemIcon);

					//if mouse position is within a rectangle
					if (slotRect.Contains (myEvent.mousePosition)) 
					{
						_toolTipInfo = CreateTooltip (_slots [indexValue]);
						_showToolTip = true;	

						//re-do for mobile
						//where right click - using - equiping items works
						if (myEvent.isMouse && myEvent.type == EventType.MouseDown && myEvent.button == 1) 
						{
							if (item._itemType == Item.ItemType.Armour) 
							{
								//the delete item ehre may be special, cause it swaps
								//UseItem (_slots[indexValue],indexValue,false);
							}
							if (item._itemType == Item.ItemType.Potion) 
							{
								UseItem (_slots[indexValue],indexValue,true);
							}
							if (item._itemType == Item.ItemType.Spell) 	
							{
								//the delete item ehre may be special, cause it swaps
								//UseItem (_slots[indexValue],indexValue,true);
							}
							if (item._itemType == Item.ItemType.Weapon) 
							{
								//the delete item ehre may be special, cause it swaps
								//UseItem (_slots[indexValue],indexValue,true);
							}
						}
						
					}
				}
				if (_toolTipInfo == "") 
				{
					_showToolTip = false;
				}


				indexValue++;

			}
		}
	}

	string CreateTooltip(Item item)
	{
		_toolTipInfo = "<color=#ffffff>" + item._strItemName + "</color>\n\n" + item._itemDesc + "\n\n";

		if (item._flItemDmg > 0) 
		{
			_toolTipInfo += "Damage: " + item._flItemDmg + "\n";
			_toolTipInfo += "Speed: " + item._flItemAttackRate + "\n\n";
		}

		_toolTipInfo += "<color=#ff69b4>" +"Sparkle Value: " + item._flItemSparkleValue + "</color>";

		return _toolTipInfo;
	}

	void UseItem(Item item, int slot, bool deleteItem)
	{
		//the id lines up with each item in the itemDataBase, meaning, if you have 100 items, each one needs to be written here
		switch (item._itItemID) 
		{
		//Sword
		case 0:
			{
				//equip player with the stats on this sword, then change slot[item] to be that of the weapon i unequipped
				break;
			}	
		//hp potion
		case 1:
			{
				//restore x hp to player
				break;
			}	
		}

		if (deleteItem) 
		{
			_inventory [slot] = new Item ();
		}

	}

	void AddItem(int id)
	{
		for (int it = 0; it < _inventory.Count; it++) 
		{
			//check for enemty slot
			if (_inventory [it]._strItemName == null) 
			{				
				_inventory [it] = _database.items [id];

				//checks entered id of new item i want to add, against database
				for (int j = 0; j < _database.items.Count; j++) 
				{
					if (_database.items [j]._itItemID == id) 
					{
						_inventory [it] = _database.items [j];
					}
				}
				break;
			}
		}
	}

	void RemoveItem(int id)
	{
		for (int it = 0; it < _inventory.Count; it++) 
		{
			if (_inventory [it]._itItemID == id) 
			{
				_inventory[it] = new Item();
				break;	
			}
		}
	}

	bool InventoryContains(int id)
	{
		bool result = false;
		for (int it = 0; it < _inventory.Count; it++) 
		{
			result = _inventory [it]._itItemID == id;
			if (result == true) 
			{
				break;
			}
		}
		return result;
	}

}
