﻿//definition of each item and their data

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item {

	public ItemType _itemType;
	public string _strItemName;
	public int _itItemID;
	public string _itemDesc;
	public float _flItemSparkleValue;
	public Texture2D _itemIcon;
	public float _flItemDmg;
	public float _flItemAttackRate;

	public enum ItemType {
		Weapon,
		Spell,
		Armour,
		Potion
	}

	public void Start()
	{
	}

	public Item(string name, int id, string desc,float damage, float speed, ItemType type)
	{
		_strItemName = name;
		_itItemID = id;
		_itemDesc = desc;
		//_itemIcon = Resources.Load<Texture2D>("Item Icons/" + name);
		//this means the name of the object needs to be identical to the image im using for it, plus, inside a Item Icons file, and that needs to be in a Resources file.
		//to get starting code in ItemDataBase, to add items, even from an xml file, we need the resources folder to exist and populate something we're trying to add into the itemdatabase(g-obj)
		_flItemDmg = damage;
		_flItemAttackRate = speed;
		_itemType = type;
	}
	public Item()
	{
		
	}
}
