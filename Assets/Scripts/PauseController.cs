﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour {

	public bool _isPaused;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{		
			//pause
			if (_isPaused == true) 
			{
				Time.timeScale = 1f;
				_isPaused = false;
				Debug.Log ("Pause");

			} 
			else if (_isPaused == false) 
			{
				Time.timeScale = 0f;	
				_isPaused = true;
				Debug.Log ("Pause");
			}

		}




	}

}
