﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeCell : MonoBehaviour {

    public Room room;
    public bool hasBeenChecked;
    public float cost;

    private void Update()
    {
        cost = room.cost;
    }

}
