﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToySpriteSwap : MonoBehaviour {

	public Sprite[] _toys;

	[Space]
	public SpriteRenderer _mySprite;

	// Use this for initialization
	void Start () 
	{
		if (Random.value > .60f) 
		{
			Destroy (gameObject);
		}


		_mySprite = gameObject.GetComponent<SpriteRenderer> ();

		for (int it = 0; it < _toys.Length; it++) {
			float randomVal = Random.value;
			if (randomVal > .86f) 
			{
				_mySprite.sprite = _toys [it];
			}
		}
	}

}
