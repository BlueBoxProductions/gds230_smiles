﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Room {

    public Vector2Int coordinates;
    public bool left, up, right, down;
    public bool startingRoom, finishingRoom;
    public int neighbours;
    public float cost;
    

    //public Room(Vector2Int c, bool l, bool r, bool u, bool d, int cost)
    //{
    //    coordinates = c;
    //    left = l;
    //    right = r;
    //    up = u;
    //    down = d;
    //    this.cost = cost;
    //}

}
