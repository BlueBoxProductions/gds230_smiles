﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour {

    public Vector2Int size, startingCoordinates;
    public int maxRoomSize;
    public MazeCell cellPrefab, cellSpawnRoomPrefab;
    public GameObject doorPrefabHori, doorPrefabVert;
    public GameObject[] cellPrefabs = new GameObject[4];
    public MazeCell[,] cells;//storing all the cells that can be made
    public List<MazeCell> cellsCreated;//storing all the cells that get created
    public List<MazeCell> cellTracker = new List<MazeCell>();
    public float generationStepDelay, roomSizeX = 1, roomSizeY = 1;
    public bool completed;
    public GameManager gm;

    public MazeCell GetCell (Vector2Int coordinates)
    {
        if (ContainsCoordinates(coordinates))
            return cells[coordinates.x, coordinates.y];
        else
            return null;
    }

    public void Generate()
    {
        //WaitForSeconds delay = new WaitForSeconds(generationStepDelay);
        cells = new MazeCell[size.x, size.y];

        List<MazeCell> activeCells = new List<MazeCell>();//Used to store ACTIVE cells meaning that when its back tracking its removing active cells from its current branch. (the cells removed are still in the orginal cells list).
        DoFirstGenerationStep(activeCells); //Change first iteration to be static as to spawn in the same spot each time.

        while (activeCells.Count > 0 && cellsCreated.Count < maxRoomSize)// If the first room has been spawned and is active. If there are no active cells the generation will stop, which would mean it hit a dead end and backtracked all the way to the start 
        {
            //yield return delay;
            DoNextGenerationStep(activeCells);
        }

        LastRoomSpawn(GetCell(startingCoordinates));
        PrefabSpawning();
        DoorCheck();
    }

    private void PrefabSpawning()
    {
        foreach (MazeCell cell in cellsCreated)
        {
            if (cell.room.startingRoom)
            {
                GameObject room = Instantiate(cellPrefabs[0]) as GameObject;
                room.transform.parent = cell.transform;
                room.transform.localPosition = new Vector2(0, 0);
            }
            else if (cell.room.finishingRoom)
            {
                GameObject room = Instantiate(cellPrefabs[1]) as GameObject;
                room.transform.parent = cell.transform;
                room.transform.localPosition = new Vector2(0, 0);
            }
            else
            {
                GameObject room = Instantiate(cellPrefabs[Random.Range(2, cellPrefabs.Length)]) as GameObject;
                room.transform.parent = cell.transform;
                room.transform.localPosition = new Vector2(0, 0);
            }
        }
    }

    private void DoFirstGenerationStep(List<MazeCell> activeCells)
    {
        startingCoordinates = new Vector2Int(0, size.y/2);
        MazeCell cell = CreateCell(startingCoordinates, true);

        gm.spawnCell = cell;
        activeCells.Add(cell);
        cellsCreated.Add(cell);

    }

    private void DoNextGenerationStep (List<MazeCell> activeCells)
    {
        int currentIndex = activeCells.Count - 1;
        MazeCell currentCell = activeCells[currentIndex];
        MazeDirection direction = MazeDirections.RandomValue;
        Vector2Int coordinates = currentCell.room.coordinates + direction.ToVector2Int();

        if (ContainsCoordinates(coordinates) && GetCell(coordinates) == null)//caps the room limit to maxRoomSize
        {
            MazeCell cell = CreateCell(coordinates, false);
            activeCells.Add(cell);
            cellsCreated.Add(cell);//tracking all cells created
        }
        else
        {
            if (activeCells.Count == 1)
            {
                List<MazeDirection> directionsAvailable = CardinalCheck(currentCell);

                if (directionsAvailable.Count > 0)//if there is any cardinal directions available from spawn.
                {
                    Vector2Int coordinatesFromSpawn = currentCell.room.coordinates + directionsAvailable[0].ToVector2Int();

                    MazeCell cell = CreateCell(coordinatesFromSpawn, false);
                    activeCells.Add(cell);
                    cellsCreated.Add(cell);//tracking all cells created
                }
                else
                {
                    activeCells.RemoveAt(currentIndex);//stops an infinite loop where it can't place anything from spawn.
                }
            }
            else
            {
                activeCells.RemoveAt(currentIndex);//backtracking
            }
        }
    }

    private void LastRoomSpawn(MazeCell cellToCheck)
    {
        List<MazeCell> highestCostCells = new List<MazeCell>();        

        float cost = 0;
        completed = true;

        foreach (MazeCell cell in cellsCreated)
        {
            cost = Vector2.Distance(startingCoordinates, cell.room.coordinates);
            cell.room.cost = cost;
        }

        for (int i = 0; i <= cellsCreated.Count-1; i++)
        {
            if(highestCostCells.Count == 0)
            {
                highestCostCells.Add(cellsCreated[i]);
            }
            else if (cellsCreated[i].room.cost == highestCostCells[highestCostCells.Count-1].room.cost)
            {
                highestCostCells.Add(cellsCreated[i]);
            }
            else if (cellsCreated[i].room.cost > highestCostCells[highestCostCells.Count-1].room.cost)
            {
                highestCostCells.Clear();
                highestCostCells.Add(cellsCreated[i]);
            }
        }

        if(completed)
        {
            if(highestCostCells.Count > 1)
            {
                highestCostCells[Random.Range(0, highestCostCells.Count - 1)].room.finishingRoom = true;
            }
            else
            {
                highestCostCells[0].room.finishingRoom = true;
            }
        }
    }

    private void DoorCheck()
    {
        foreach (MazeCell cell in cellsCreated)
        {
            Vector2Int coordsNorth = cell.room.coordinates + MazeDirection.North.ToVector2Int();
            Vector2Int coordsEast = cell.room.coordinates + MazeDirection.East.ToVector2Int();
            Vector2Int coordsSouth = cell.room.coordinates + MazeDirection.South.ToVector2Int();
            Vector2Int coordsWest = cell.room.coordinates + MazeDirection.West.ToVector2Int();

            if (ContainsCoordinates(coordsNorth) && GetCell(coordsNorth) != null)
            {
                Transform topSpawnLocation = cell.transform.GetChild(0).transform.Find("Top-Doorspawn");
                if (topSpawnLocation != null)
                    SpawnDoor(topSpawnLocation, cell, 180, doorPrefabHori, GetCell(coordsNorth));
            }
            if (ContainsCoordinates(coordsEast) && GetCell(coordsEast) != null)
            {
                Transform rightSpawnLocation = cell.transform.GetChild(0).transform.Find("Right-Doorspawn");
                if (rightSpawnLocation != null)
                    SpawnDoor(rightSpawnLocation, cell, 180, doorPrefabVert, GetCell(coordsEast));
            }
            if (ContainsCoordinates(coordsSouth) && GetCell(coordsSouth) != null)
            {
                Transform bottomSpawnLocation = cell.transform.GetChild(0).transform.Find("Bottom-Doorspawn");
                if (bottomSpawnLocation != null)
                    SpawnDoor(bottomSpawnLocation, cell, 0, doorPrefabHori, GetCell(coordsSouth));
            }
            if (ContainsCoordinates(coordsWest) && GetCell(coordsWest) != null)
            {
                Transform leftSpawnLocation = cell.transform.GetChild(0).transform.Find("Left-Doorspawn");
                if (leftSpawnLocation != null)
                    SpawnDoor(leftSpawnLocation, cell, 0, doorPrefabVert, GetCell(coordsWest));
            }
        }        
    }

    private void SpawnDoor(Transform doorLocation, MazeCell parentCell, int rotation, GameObject prefab, MazeCell _cellGoingTo)
    {
        GameObject door = Instantiate(prefab) as GameObject;
        door.transform.localScale = new Vector3(1, 1, 1);
        door.transform.parent = parentCell.transform;
        door.transform.Rotate(Vector3.forward, rotation);
        door.transform.position = doorLocation.position;
        door.GetComponent<DoorTrigger>().cellGoingTo = _cellGoingTo.transform.position;
    }

    private List<MazeDirection> CardinalCheck(MazeCell cellToCheck)//used to get a list of the surrounding available cells at spawn.
    {
        List<MazeDirection> directionsAvailable = new List<MazeDirection>();

        Vector2Int coordsNorth = cellToCheck.room.coordinates + MazeDirection.North.ToVector2Int();
        Vector2Int coordsEast = cellToCheck.room.coordinates + MazeDirection.East.ToVector2Int();
        Vector2Int coordsSouth = cellToCheck.room.coordinates + MazeDirection.South.ToVector2Int();
        Vector2Int coordsWest = cellToCheck.room.coordinates + MazeDirection.West.ToVector2Int();

        if (ContainsCoordinates(coordsNorth) && GetCell(coordsNorth) == null)
        {
            directionsAvailable.Add(MazeDirection.North);
        }
        if (ContainsCoordinates(coordsEast) && GetCell(coordsEast) == null)
        {
            directionsAvailable.Add(MazeDirection.East);
        }
        if (ContainsCoordinates(coordsSouth) && GetCell(coordsSouth) == null)
        {
            directionsAvailable.Add(MazeDirection.South);
        }
        if (ContainsCoordinates(coordsWest) && GetCell(coordsWest) == null)
        {
            directionsAvailable.Add(MazeDirection.West);
        }

        return directionsAvailable;
    }

    public bool ContainsCoordinates (Vector2Int c)//if the chosen coordinates is inside the boundaries.
    {
        return c.x >= 0 && c.x <= size.x && c.y >= 0 && c.y <= size.y;
    }

    private MazeCell CreateCell(Vector2Int coordinates, bool firstRoom)//instantiates the cell with the given properties.
    {
        MazeCell newCell;

        if (firstRoom)
        {
            newCell = Instantiate(cellSpawnRoomPrefab) as MazeCell;
            newCell.room.startingRoom = true;
        }
        else
        {
            newCell = Instantiate(cellPrefab) as MazeCell;
        }

        cells[coordinates.x, coordinates.y] = newCell;
        newCell.room.coordinates = coordinates;
        newCell.name = "Maze Cell " + coordinates.x + ", " + coordinates.y;
        newCell.transform.parent = transform;
        newCell.transform.localPosition = new Vector2((coordinates.x * roomSizeX) - (size.x * roomSizeX) * 0.5f, (coordinates.y * roomSizeY) - (size.y * roomSizeY) * 0.5f);//coords are multiplied by roomsize to expand outwards, this allows the rooms to have an ajustable gap.

        return newCell;
    }

    //pseudo code:
    //run through the list of created cells in order.
    //the first spawn room will be a 1x1 room that the player spawns in.
    //the next room will check if it has 3 cells next to it in north-west-south-east (in that order) 
    //if it does it will spawn a 2x2 room and flag those cells with a marker delcaring that it has been modified.
    //continue to go through the rest of the cells and modify all possible cells into 2x2
    //cells that cant be changed will stay as 1x1

    //add the function to make 1x2 and 2x1 cells.//maybe make it a % chance, like 70% chance to spawn thses rooms.

}
