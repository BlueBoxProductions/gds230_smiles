﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

	public bool  _hasEnded = false;
	float _fadeTimer;
	public PlayerController _playerRef;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_hasEnded == true) 
		{
			_playerRef._canMove = false;

			float fadeTime = GameObject.Find ("Game Manager").GetComponent<Fading> ().BeginFade (1);
			SpriteRenderer player = _playerRef.GetComponent<SpriteRenderer> ();
			Color invis = player.color;
			invis.a = 0f;
			player.color = invis;



			_fadeTimer += Time.deltaTime;

			if (_fadeTimer >= 1f) 
			{
				_fadeTimer = 0f;
				_hasEnded = false;
				SceneManager.LoadScene ("MainMenu");
			}
		}
	}

	public void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") 
		{
			_playerRef = col.GetComponent<PlayerController> ();

			PermanentManager._isGameOver = true;
			PermanentManager._flCurrentSparkles = _playerRef._flSparkleValue;
		}
		_hasEnded = true;
	}
}
