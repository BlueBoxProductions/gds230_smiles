﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public Maze mazePrefab;
    public MazeCell spawnCell;

    private Maze mazeInstance;
    private int generationAttempts = 0;

    private void Start()
    {
        BeginGame();
    }

    private void BeginGame()
    {
        mazeInstance = Instantiate(mazePrefab) as Maze;
        mazeInstance.gm = this.GetComponent<GameManager>();
        mazeInstance.Generate();
        if (generationAttempts == 20)
        {
            Debug.Log("Trying again : #" + generationAttempts);
            return;
        }
        else if (mazeInstance.cellsCreated.Count != mazeInstance.maxRoomSize || !mazeInstance.completed)
        {
            RestartGame();
            generationAttempts++;
        }
    }

    private void RestartGame()
    {
        Destroy(mazeInstance.gameObject);
        BeginGame();
    }
}
