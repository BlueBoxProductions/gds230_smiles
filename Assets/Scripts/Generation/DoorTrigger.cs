﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour {

    public Transform otherSide;
    public Vector3 cellGoingTo;//cell that the door leads to

    public void Start()
    {
        otherSide = transform.Find("Teleport transform");
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.transform.position = otherSide.position;
            collision.GetComponent<PlayerController>().roomLocation = cellGoingTo;
        }
    }
}
