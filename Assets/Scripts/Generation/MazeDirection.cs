﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MazeDirection {
    
    North,
    East,
    South,
    West

}

public static class MazeDirections {

    public const int Count = 4;

    public static MazeDirection RandomValue
    {
        get
        {
            return (MazeDirection)Random.Range(0, Count);
        }
    }

    private static Vector2Int[] vectors = {
        new Vector2Int (0, 1),
        new Vector2Int (1, 0),
        new Vector2Int (0, -1),
        new Vector2Int (-1, 0)
    };

    public static MazeDirection OppositeDirection (this MazeDirection direction)
    {
        MazeDirection oppDirection = direction;

        switch (direction)
        {
            case MazeDirection.North:
                oppDirection = MazeDirection.South;
                break;
            case MazeDirection.East:
                oppDirection = MazeDirection.West;
                break;
            case MazeDirection.South:
                oppDirection = MazeDirection.North;
                break;
            case MazeDirection.West:
                oppDirection = MazeDirection.East;
                break;
        }

        return oppDirection;
    }

    public static Vector2Int ToVector2Int (this MazeDirection direction)
    {
        return vectors[(int)direction];
    }

    public static Vector2Int ToVector2Int(int index)
    {
        return vectors[index];
    }

}