﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermanentManager : MonoBehaviour {

	// Permanisation

	public static PermanentManager _instance = null;

	void Awake() {

		if (_instance == null) {
			_instance = this;
		} else {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);

	}

	// Player Manager

	public static float _flCurrentSparkles = 0; // The player's collection of Sparkles from the Dungeon they're currently running.

	public static float _flTotalSparkles = 0; // The player's total Sparkle Amount. Should only be changed from the Main Menu.
	public static float _flPremium = 0; // The player's current real money investment into the game.

	public static int _itPremiumBoost = 0; // Whether we've paid for a Premium Dungeon or not, and how many we've paid for.

	public static bool _isGameOver = false; // Whether we just finished a dungeon run or not.

}
