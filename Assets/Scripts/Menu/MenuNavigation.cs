﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuNavigation : MonoBehaviour {

	[Space] [Header("Monetisation Statistics")]

	[Space] 
	[SerializeField] private float _flDungeonBonusMax;
	[SerializeField] private float _flDungeonBonusMin;

	[Tooltip("How much we increase the potential winnings of the random chance when the player purchases a reroll.")]
	[SerializeField] private float _flDungeonBonusRerollOffset;

	[Space] [Header("Panels to activate")]
	[SerializeField] private GameObject _gmSocialMenu;
	[SerializeField] private GameObject _gmStoreMenu;
	[SerializeField] private GameObject _gmStoreMenuPremium;

	[Space] [Header("Store Elements")]
	[SerializeField] private TextMeshProUGUI _txStoreSparkleCounter;
	[SerializeField] private TextMeshProUGUI _txStorePremiumCounter;

	[Space] [Header("Dialogue Boxes")]
	[SerializeField] private GameObject _gmLackOfPremium; // The "You don't have enough Premium for this action" pop up box.
	[SerializeField] private GameObject _gmLackOfSparkles; // " " " Sparkles
	[SerializeField] private GameObject _gmPurchaseConfirm;

	[Space]
	[SerializeField] private TextMeshProUGUI _txConfirmQuestion;

	[Space] [Header("Game Over Menu Resources")]
	[SerializeField] private GameObject _gmGameOverMenu;
	[SerializeField] private GameObject _gmReroll; // The Reroll Button

	[Space] 
	[SerializeField] private TextMeshProUGUI _txGameOverSparkles;
	[SerializeField] private TextMeshProUGUI _txGameOverPrem;
	[SerializeField] private TextMeshProUGUI _txGameOverDun;
	[SerializeField] private TextMeshProUGUI _txGameOverFinal;

	[Space] [Header("Fancy Effects")]
	[SerializeField] private GameObject _gmParticles; // Particle Effects to spawn!

	// Currency Variables
	[Space] [Header("Currency Variables")]
	[SerializeField] private float _flSparkles;
	[SerializeField] private float _flPremium;

	private float _flLastDungeonSparkles;

	// Purchasing Variables
	private string _stPurchaseName;
	private float _flPurchasePrice;
	private bool _isPurchaseSparkles;
	private int _itPurchaseBoosts;

	// High Priority Tasks
	private void Awake () {

		// First Assign permanent variables to local files.
		_flLastDungeonSparkles = PermanentManager._flCurrentSparkles;
		_flSparkles = PermanentManager._flTotalSparkles;
		_flPremium = PermanentManager._flPremium;

		// Check to see if we just ended a Dungeon. If so, we need to bring up the game over screen.
		if (PermanentManager._isGameOver == true) {

			float flPrem;
			float flDun;

			_gmGameOverMenu.SetActive (true);
			PermanentManager._isGameOver = false;

			flDun = Random.Range(_flDungeonBonusMin,_flDungeonBonusMax);

			if (PermanentManager._itPremiumBoost >= 1) {
				flPrem = 2.5f;
				PermanentManager._itPremiumBoost--;
			} else {
				flPrem = 1.0f;
			}

			_txGameOverSparkles.text = _flLastDungeonSparkles.ToString();
			_txGameOverPrem.text = flPrem.ToString("F1");
			_txGameOverDun.text = flDun.ToString("F1");

			_txGameOverFinal.text = (_flLastDungeonSparkles * flPrem * flDun).ToString("F0") + " Sparkles!";

		}

	}

	// Less important tasks.
	private void Start () {

	}

	// Reroll Dungeon Bonus Multiplier
	public void BonusReroll () {

		if (_flPremium >= 50f) {

			_flPremium -= 50f;
			_gmReroll.SetActive (false);

			float flDun = Random.Range (_flDungeonBonusMin + _flDungeonBonusRerollOffset, _flDungeonBonusMax + _flDungeonBonusRerollOffset);
			float flFinal = _flLastDungeonSparkles * (PermanentManager._itPremiumBoost >= 1 ? 2.5f : 1.0f) * flDun;

			_txGameOverDun.text = flDun.ToString("F1");
			_txGameOverFinal.text = flFinal.ToString("F0") + " Sparkles!";


		} else {

			_gmLackOfPremium.SetActive (true);

		}

	}

	// Start Button to Gameplay
	public void Play () {

		// Allocate resources back to Permanent Manager
		PermanentManager._flPremium = _flPremium;
		PermanentManager._flTotalSparkles = _flSparkles;
		PermanentManager._flCurrentSparkles = 0f;

		// Finally, switch scenes
		SceneManager.LoadScene("Dungeon");

	}

	// Update "Total Sparkle" amount with Current Sparkles and clear values.
	public void UpdateTotals () {

		_flSparkles += _flLastDungeonSparkles;
		_flLastDungeonSparkles = 0f;

	}

	// Update text elements that hold Premium and Sparkle Values
	public void UpdateElements () {

		_txStorePremiumCounter.text = _flPremium.ToString ("F0");
		_txStoreSparkleCounter.text = _flSparkles.ToString ("F0");

	}

	// Screen Movement Issues to 
	public void PanelChange (int it) {

		switch (it) {

		// Social
		case (0):
			_gmSocialMenu.SetActive (_gmSocialMenu.activeInHierarchy ? false : true);
			break;
		
		// Store
		case (1):
			_gmStoreMenu.SetActive (_gmStoreMenu.activeInHierarchy ? false : true);
			break;

		// Game Over
		case (2):
			_gmStoreMenuPremium.SetActive (_gmStoreMenuPremium.activeInHierarchy ? false : true);
			break;

		case (3):
			_gmGameOverMenu.SetActive (_gmGameOverMenu.activeInHierarchy ? false : true);
			break;

		}

	}

	// Dialogue Box Issued to Player
	public void DialogueBox (int it) {

		switch (it) {

		// Not Enough Premium
		case (0):
			_gmLackOfPremium.SetActive (_gmLackOfPremium.activeInHierarchy ? false : true);
			break;

		case (1):
			_gmLackOfSparkles.SetActive (_gmLackOfSparkles.activeInHierarchy ? false : true);
			break;

		case (2):
			_gmPurchaseConfirm.SetActive (_gmPurchaseConfirm.activeInHierarchy ? false : true);
			_itPurchaseBoosts = 0;
			break;

		}

	}

	// Part one of Shop Purchase Exodia
	public void PurchaseNamer (string stName) {

		_stPurchaseName = stName;

	}

	// Part Two
	public void PurchaseType (bool isSparkles) {

		_isPurchaseSparkles = isSparkles;

	}

	// Optional Section for Dungeon Boosters to add to Payment
	public void PurchaseBoosters (int itBoosts) {

		_itPurchaseBoosts = itBoosts;

	}

	// Part Three
	public void PurchaseConfirmer (int itPrice) {

		float flWallet = _isPurchaseSparkles ? _flSparkles : _flPremium;

		if (flWallet >= itPrice) {

			_txConfirmQuestion.text = string.Format ("Are you sure you want to purchase the {0} for {1}? You have {2} {3} remaining.", _stPurchaseName, itPrice.ToString ("F0"), _isPurchaseSparkles ? _flSparkles : _flPremium, _isPurchaseSparkles ? " Sparkles?" : " Premium?");

			_gmPurchaseConfirm.SetActive (true);
			_flPurchasePrice = itPrice;

		} else {

			if (_isPurchaseSparkles) {
				_gmLackOfSparkles.SetActive (true);
			} else {
				_gmLackOfPremium.SetActive (true);
			}

		}

	}

	// Shop Purchase Deduction and Finalisation
	public void Purchaser () {

		if (_isPurchaseSparkles) {
			_flSparkles -= _flPurchasePrice;
		} else {
			_flPremium -= _flPurchasePrice;
		}

		PermanentManager._itPremiumBoost += _itPurchaseBoosts;
		_itPurchaseBoosts = 0;

		_gmPurchaseConfirm.SetActive (false);
		UpdateElements ();

		Debug.Log (PermanentManager._itPremiumBoost);

	}

	// Add Premium to Account
	public void PremiumAdd () {

		_flPremium += 1000;
		UpdateElements ();

	}
		
}