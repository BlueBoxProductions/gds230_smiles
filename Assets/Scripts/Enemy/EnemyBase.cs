﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour {
	
	[SerializeField] SpriteRenderer _mySprite;

	[Header("Enemy Settings")]
	[SerializeField] private float _flSpeed = 3f;

	[Space]
	[SerializeField] public float _flMaxHealth = 3f;
	[SerializeField] public float _flCurHealth = 3f;
	public float _flSparkleValue;
	[Space]
	[SerializeField] public bool _isDead = false;
	[SerializeField] GameObject _deathFX;


	[Header("Combat")]

	public GameObject _playerRef;
	[SerializeField] bool _canBasicAttack = true;
	[SerializeField] float _baseDamage = 1f;
	[SerializeField] float _myfireRate;
	public GameObject _myAttack;

	public GameObject _itemToDrop;

	// Use this for initialization
	void Start () 
	{
		_myfireRate = Random.Range (1.2f, 2.8f);
		_mySprite = this.GetComponent<SpriteRenderer> ();
	}

	public void Update()
	{
		if (Vector3.Distance(_playerRef.transform.position,gameObject.transform.position) <= 5f) 
		{
			if (_canBasicAttack == true) 
			{
				StartCoroutine (BasicAttack(_baseDamage));
			}
			
		}

	}

	public void TakeHealth(float takeAmount)
	{		
		_flCurHealth = _flCurHealth - takeAmount;
		StartCoroutine (FlashDamage());

		//death / dead
		if (_flCurHealth <= 0f) 
		{
			_playerRef.GetComponent<PlayerController> ()._flSparkleValue += _flSparkleValue;
			_isDead = true;	

			Destroy( Instantiate (_deathFX, transform.position, transform.rotation), 5f);

			// chance of dropping your item = number in random value
			if (Random.value <= .50f) 
			{
				Instantiate (_itemToDrop, transform.position, transform.rotation);
				
			}
		}

	}

	public IEnumerator BasicAttack(float damage)
	{		
		_canBasicAttack = false;
		yield return new WaitForSeconds (_myfireRate);

		GameObject spell;
		spell = Instantiate (_myAttack, this.transform.position, this.transform.rotation) as GameObject;
		spell.GetComponent<EnemyAttack> ()._flDamage = _baseDamage;
		spell.GetComponent<EnemyAttack> ()._ebOrigin = this.gameObject.GetComponent<EnemyBase>();

		//https://docs.unity3d.com/Manual/DirectionDistanceFromOneObjectToAnother.html
		Vector3 heading = _playerRef.transform.position - this.gameObject.transform.position;
		heading = heading.normalized;
		float distance = heading.magnitude;
		Vector3 direction = heading / distance;

		spell.GetComponent<Rigidbody2D> ().AddForce(direction * 250f);


		_canBasicAttack = true;	

	}

	public IEnumerator FlashDamage()
	{
		Color flash = _mySprite.color;

		for (int it = 0; it < 3; it++) 
		{		
			flash.a = 0f;
			yield return new WaitForSeconds (.1f);
			_mySprite.color = flash;
			flash.a = 1f;
			yield return new WaitForSeconds (.1f);
			_mySprite.color = flash;
		}

	}

	public void LateUpdate()
	{

		gameObject.transform.up = Vector3.up;
	}
}
