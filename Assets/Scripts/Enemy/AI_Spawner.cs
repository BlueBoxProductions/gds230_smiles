﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Spawner : MonoBehaviour {

	[SerializeField] GameObject _player;
	public GameObject _lootTableRef;

	public GameObject[] _enemiesToSpawn;

	[SerializeField] Vector3 _spawnVariance;
	public float _spawnValue;
	// Use this for initialization
	void Awake () {

		_player = GameObject.FindGameObjectWithTag ("Player");
		_lootTableRef = GameObject.FindGameObjectWithTag ("LootTable");

		_spawnValue = Random.value;
		// 90% chance to spawn enemies

		if (_spawnValue >= 0.10f) {

			for (int it = 0; it < Random.Range (4, 6); it++) 
			{
				_spawnVariance.x = _spawnVariance.x + Random.Range (-2f, 2f);
				if (_spawnVariance.x < -10f || _spawnVariance.x > 10f) 
				{
					_spawnVariance.x = 0f + Random.Range (-2f, 2f);
				}

				_spawnVariance.y = _spawnVariance.y + Random.Range (-2f, 2f);
				if (_spawnVariance.y < -2f || _spawnVariance.y > 2f) 
				{
					_spawnVariance.y = 0f + Random.Range (-2f, 2f);
				}

				_spawnVariance.x = (int)_spawnVariance.x;
				_spawnVariance.y = (int)_spawnVariance.y;


				GameObject enemy;
				enemy = Instantiate (_enemiesToSpawn [Random.Range (0, _enemiesToSpawn.Length)], this.transform.position + _spawnVariance, this.transform.rotation, gameObject.transform) as GameObject;
				enemy.GetComponent<EnemyBase> ()._playerRef = _player;
				enemy.GetComponent<EnemyBase> ()._itemToDrop = _lootTableRef.GetComponent<ItemLootTable>()._items [Random.Range (0, _lootTableRef.GetComponent<ItemLootTable>()._items.Length)];

			}

		} 
		else 
		{
			GameObject enemy;
			enemy = Instantiate (_enemiesToSpawn [Random.Range (0, _enemiesToSpawn.Length)], this.transform.position + _spawnVariance, this.transform.rotation, gameObject.transform) as GameObject;
			enemy.GetComponent<EnemyBase> ()._playerRef = _player;
			enemy.GetComponent<EnemyBase> ()._itemToDrop = _lootTableRef.GetComponent<ItemLootTable>()._items [Random.Range (0, _lootTableRef.GetComponent<ItemLootTable>()._items.Length)];
		}

	}

}
