﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoy : EnemyBase {

	public GameObject _goodVersion;

	public void Update()
	{
		base.Update ();

		if (base._isDead == true) 
		{
			Instantiate (_goodVersion, transform.position, transform.rotation);
			Destroy (this.gameObject);
		}
	}

}
