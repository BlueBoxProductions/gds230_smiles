﻿
/* --------------------------------------------------------------------------------------------------------------------------------------------------------- //
  	Author: 		Eric Cox
	File:			BulletController.cs
	Description: 	This script controls the player's base spell's.
// --------------------------------------------------------------------------------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {	

	[Header("Spell Variables")]
	[SerializeField] public float _flDamage;
	public EnemyBase _ebOrigin;

	void Start () 
	{
		StartCoroutine (Destroy ());

		transform.rotation = Quaternion.LookRotation (_ebOrigin.transform.position);
	}

	void Update()
	{


	}

	public void OnTriggerEnter2D (Collider2D col)
	{

		if (col.gameObject.tag == "Player") 
		{
			col.gameObject.GetComponent<PlayerController> ().TakeHealth (_flDamage);
			//col.gameObject.GetComponent<EnemyBase> ().TakeDamage (_flDamage);
			Destroy (gameObject);
		}
		if (col.gameObject.tag == "Wall") 
		{
			Destroy (gameObject);
		}
	}

	// After a delay and having hit nothing, destroy the bullet.
	IEnumerator Destroy() 
	{
		yield return new WaitForSeconds (3f);
		Destroy(gameObject);
	}
}
