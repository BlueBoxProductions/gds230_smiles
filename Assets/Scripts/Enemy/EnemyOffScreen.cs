﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOffScreen : MonoBehaviour {

	float _direction;
	SpriteRenderer _mySprite;

	// Use this for initialization
	void Start () {


		_mySprite = gameObject.GetComponent<SpriteRenderer> ();

		Destroy (this.gameObject, 3f);
		_direction = Random.value;
		if (_direction > 0.5f) 
		{
			_direction = 1f;
			_mySprite.flipX = false;
		}
		else if (_direction <= 0.5f) 
		{
			_direction = -1f;	
			_mySprite.flipX = true;
		}


	}
	
	// Update is called once per frame
	void Update () {


		transform.Translate (_direction * 2 * Time.deltaTime,0f, 0f);
	}


}
