﻿
/* --------------------------------------------------------------------------------------------------------------------------------------------------------- //
  	Author: 		Eric Cox
	File:			BulletController.cs
	Description: 	This script controls the player's base spell's.
// --------------------------------------------------------------------------------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAttack : MonoBehaviour {	

	[Header("Spell Variables")]
	public string _strSpellName;
	public Sprite _sprSpell;
	[SerializeField] public float _flDamage;
	public float _flSpeed;
	public float _flAttackRate;
	public float _flManaCost;

	public PlayerController _pcOrigin;

	public void OnTriggerEnter2D (Collider2D col)
	{	

		if (col.gameObject.tag == "Enemy") 
		{
			col.gameObject.GetComponent<EnemyBase> ().TakeHealth (_flDamage);
			Destroy (this.gameObject.GetComponentInChildren<SpriteRenderer> (),0.1f);
			Destroy (this);
		}

		if (col.gameObject.tag == "Wall") 
		{
			Destroy (this.gameObject.GetComponentInChildren<SpriteRenderer> (),0.1f);
			Destroy (this);
		}
	}

}
