﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAttack : MonoBehaviour {

	public string _strNme;
	public string _strType;
	public float _fldmg;

	public float _flAttackRate;
	public float _flSparkleValue;

	public void Start()
	{

		StartCoroutine (Destroy ());
	}

	public void OnTriggerEnter2D (Collider2D col)
	{	

		if (col.gameObject.tag == "Enemy") 
		{
			col.gameObject.GetComponent<EnemyBase> ().TakeHealth (_fldmg);
		}
	}

	// After a delay and having hit nothing, destroy the weapon hitbox
	IEnumerator Destroy() 
	{
		yield return new WaitForSeconds (.6f);
		Destroy(gameObject);
	}

}
