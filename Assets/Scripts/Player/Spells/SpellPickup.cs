﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellPickup : MonoBehaviour {
	
	public PlayerController _playerRef;
	public GameObject _currentSpell;
	public Sprite _sprCurrentSpell;
	public GameObject _newSpell;
	public Sprite _sprNewSpell;

	public SpriteRenderer _mySprite;
	[SerializeField] Color _newColor;
	[SerializeField] Color _oldColor;


	bool _enabled = false;

	public void Awake()
	{
		_mySprite = gameObject.GetComponent<SpriteRenderer> ();
		_playerRef = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();

	}

	public void Update()
	{
		if (_enabled == true) 
		{
		
//			if (Input.GetKeyDown (KeyCode.E)) 
//			{
//				//decalre temp values to hold and swap images and game objects later
//				GameObject gbTemp;
//				Sprite sprTemp;
//
//				//grab the spell and sprite off the player and put on this this object
//				_sprCurrentSpell = _currentSpell.GetComponent<BaseAttack> ()._sprSpell;
//				_newSpell = _playerRef._equippedSpell;
//				_sprNewSpell = _playerRef._equippedSpell.GetComponent<BaseAttack> ()._sprSpell;
//
//				//sets the players spell as to what was on this object
//				_playerRef._equippedSpell = _currentSpell;
//				_playerRef._txtSpellName.text = _currentSpell.name;
//
//				//sets the temp variables as what the player had, and declares that as the new spell, and swaps the two. ~
//				// ~ so the 'new spell' entered on this object is the current one and the player can gain this gameobject again
//				gbTemp = _newSpell;
//				sprTemp = _newSpell.GetComponent<BaseAttack> ()._sprSpell;
//				_newSpell = _currentSpell;
//				_sprNewSpell = _currentSpell.GetComponent<BaseAttack> ()._sprSpell;
//				_currentSpell = gbTemp;
//				_sprCurrentSpell = sprTemp;
//
//				//finally sets this sprite to the appropriate spell
//				_mySprite.sprite = _sprCurrentSpell;
//				_mySprite.color = _oldColor;
//				_enabled = false;
//
//				//transform.position = _playerRef.transform.position;
//			}
		}

	}

	public void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") 
		{
			//decalre temp values to hold and swap images and game objects later
			GameObject gbTemp;
			Sprite sprTemp;

			//grab the spell and sprite off the player and put on this this object
			_sprCurrentSpell = _currentSpell.GetComponent<BaseAttack> ()._sprSpell;
			_newSpell = _playerRef._equippedSpell;
			_sprNewSpell = _playerRef._equippedSpell.GetComponent<BaseAttack> ()._sprSpell;

			//sets the players spell as to what was on this object
			_playerRef._equippedSpell = _currentSpell;
			_playerRef._txtSpellName.text = _currentSpell.name;

			//sets the temp variables as what the player had, and declares that as the new spell, and swaps the two. ~
			// ~ so the 'new spell' entered on this object is the current one and the player can gain this gameobject again
			gbTemp = _newSpell;
			sprTemp = _newSpell.GetComponent<BaseAttack> ()._sprSpell;
			_newSpell = _currentSpell;
			_sprNewSpell = _currentSpell.GetComponent<BaseAttack> ()._sprSpell;
			_currentSpell = gbTemp;
			_sprCurrentSpell = sprTemp;

			//finally sets this sprite to the appropriate spell
			_mySprite.sprite = _sprCurrentSpell;
			_mySprite.color = _oldColor;
			_enabled = false;
		}
		//transform.position = _playerRef.transform.position;


//		if (col.tag == "Player") {
//			_playerRef = col.GetComponent<PlayerController> ();
//			_mySprite.color = _newColor;
//			_enabled = true;
//		}
	}

	public void OnTriggerExit2D(Collider2D col)
	{
//		if (col.tag == "Player") {
//			_mySprite.color = _oldColor;
//			_enabled = false;
//		}
	}

}
