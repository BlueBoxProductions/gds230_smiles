﻿// Author: Eric Cox
// Contributors: 
// Purpose: The player controller for Magical Wizard Girl

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	[Header("Player Animation")]
	[SerializeField] Animator animator;
	[SerializeField] SpriteRenderer _mySprite;

	[Header("Player Settings")]
    [SerializeField] private float _flSpeed = 3f;
    public Vector3 roomLocation;

    [Space]

	public bool _canMove = false;
	[SerializeField] bool _dashing = false;
	[SerializeField] Vector3 _dashDir;
	[SerializeField] float _dashCD = 0f;
	[SerializeField] bool _canDash = true;
	[SerializeField] float _dashTimer = 0f;

	[Space]
	[SerializeField] private float _flMaxHealth = 20f;
	[SerializeField] public float _flCurHealth = 20f;

	[Space]
	public bool _isDead = false;

	[Space]
	[SerializeField] private float _flMaxMana = 12f;
	[SerializeField] public float _flCurMana = 12f;
	[SerializeField] private float _flRegenSpeed = .1f;
	[SerializeField] private float _flRegenAmount = .2f;

	[Space]

	[Header("Combat")]

	public GameObject _equippedSpell;
	//public GameObject[] _primarySpell;

	[Space]
	public float _flSpellCD;
	public int _itCurSpell = 0;
	[SerializeField] bool _canCastSpell = true;
	[SerializeField] float _spellDamage = 1f;

	[Space]
	public GameObject _weaponAttack;
	public float _flweaponCD;
	[SerializeField] bool _canWeaponAttack = true;
	[SerializeField] float _weaponDmg = 1f;

	[Space]

	[Header("Death")]
	[SerializeField] GameObject _deathFX;
	[SerializeField] float _fadeTimer;
	bool _hasSparkled = false;

	[Header("Player UI Elements")]
	[SerializeField] Slider _slHealthBar;
	[SerializeField] Slider _slManaBar;
	[SerializeField] public Text _txtSpellName;


	[Space]
	[Header("Player Inventory")]
	public float _flSparkleValue;

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	void Start () 
	{
		_canMove = true;
		_mySprite = GetComponent<SpriteRenderer> ();
		animator = GetComponent<Animator> ();

		transform.tag = "Player";

		_slHealthBar.maxValue = _flMaxHealth;
		_slHealthBar.value = _flCurHealth;

		_slManaBar.maxValue = _flMaxMana;
		_slManaBar.value = _flCurMana;
		StartCoroutine (RegenMana ());

		if (_equippedSpell != null) 
		{
			_txtSpellName.text = _equippedSpell.GetComponent<BaseAttack> ()._strSpellName;
			_spellDamage = _equippedSpell.GetComponent<BaseAttack> ()._flDamage;
			_flSpellCD = _equippedSpell.GetComponent<BaseAttack> ()._flAttackRate;
		}

	}


	public Vector3 attackVec;
	void Update ()
	{
		//________________________________________________ ATTACKING ___________________________________________________________//



		#if UNITY_EDITOR_WIN
		if (Input.GetKey(KeyCode.Mouse0) && _canMove == true) 
		{

			_spellDamage = _equippedSpell.GetComponent<BaseAttack> ()._flDamage;
			
			if(_canWeaponAttack == true)
			{
				StartCoroutine (CastSpellMouse(_spellDamage));
			}
		}
		#endif


		#if UNITY_ANDROID
		float flAttackXAxis = CrossPlatformInputManager.GetAxis ("Horizontal_2");
		float flAttackYAxis = CrossPlatformInputManager.GetAxis ("Vertical_2");
		//spell attack
		attackVec = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal_2"),CrossPlatformInputManager.GetAxis("Vertical_2"));			
		if (attackVec.x != 0f || attackVec.y != 0f ) 
		{
			if(_canMove == true)
			{
				_spellDamage = _equippedSpell.GetComponent<BaseAttack> ()._flDamage;
				if(_canWeaponAttack == true)
				{
					StartCoroutine (CastSpell(_spellDamage));
				}
			}
		}

		#endif

		// weapon attack - right click
//		if (Input.GetKey (KeyCode.Mouse1)) 
//		{
//			if(_canCastSpell == true)
//			{
//				StartCoroutine (WeaponAttack(_weaponAttack.GetComponent<WeaponAttack>()._fldmg));
//			}
//		}


		//________________________________________________vv DASH vv___________________________________________________________//

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			if (_canDash == true) 
			{
				_dashing = true;
				Vector3 v3Pos = Camera.main.WorldToScreenPoint (transform.position);
				Vector3 v3Dir = (Input.mousePosition - v3Pos).normalized;
				_dashDir = v3Dir;
				_canDash = false;
			}

		}

		if(_dashing == true)
		{
			_dashTimer += Time.deltaTime;

			transform.Translate (_dashDir.x * _flSpeed * 2f * Time.deltaTime, _dashDir.y * _flSpeed * 2f * Time.deltaTime, 0f);		

			if (_dashTimer >= .3f) 
			{
				_dashing = false;
				_dashTimer = 0f;
			}

		}

		if (_canDash == false) 
		{
			_dashCD += Time.deltaTime;

			if (_dashCD >= 1f) 
			{
				_canDash = true;
				_dashCD = 0f;
			}
		
		}

		//________________________________________________^^ DASH ^^___________________________________________________________//


		//________________________________________________ MOVEMENT___________________________________________________________//

		// 0 - Idle
		//1 - Upwards
		//2 - right
		//3 - down
		//4 - left

		#if UNITY_ANDROID

		float flXAxisMobile = CrossPlatformInputManager.GetAxis ("Horizontal");
		float flYAxisMobile = CrossPlatformInputManager.GetAxis ("Vertical");

		if (flYAxisMobile > 0f) 
		{
			animator.SetInteger ("Direction", 1);
		}
		else if (flYAxisMobile < 0f)
		{
			animator.SetInteger ("Direction", 3);
		}
		else if (flXAxisMobile > 0f)
		{
			animator.SetInteger ("Direction", 2);
		}
		else if (flXAxisMobile < 0f)
		{
			animator.SetInteger ("Direction", 4);
		}

		//if (flXAxis == 0f && flYAxis == 0f || flXAxisMobile == 0f && flYAxisMobile == 0f) 
		if (flXAxisMobile == 0f && flYAxisMobile == 0f) 
		{
			animator.SetInteger ("Direction", 0);
			animator.speed = .2f;
		} 
		else 
		{
			animator.speed = .8f;
		}
		if (!_dashing) 
		{
			transform.Translate (flXAxisMobile * _flSpeed * Time.deltaTime, flYAxisMobile * _flSpeed * Time.deltaTime, 0f);

		}
		#endif

		#if UNITY_EDITOR_WIN

		float flYAxis = Input.GetAxis ("Vertical");
		float flXAxis = Input.GetAxis ("Horizontal");

		if (flYAxis > 0f) 
		{
			animator.SetInteger ("Direction", 1);
		}
		else if (flYAxis < 0f)
		{
			animator.SetInteger ("Direction", 3);
		}
		else if (flXAxis > 0f)
		{
			animator.SetInteger ("Direction", 2);
		}
		else if (flXAxis < 0f)
		{
			animator.SetInteger ("Direction", 4);
		}

		//if (flXAxis == 0f && flYAxis == 0f || flXAxisMobile == 0f && flYAxisMobile == 0f) 
		if (flXAxis == 0f && flYAxis == 0f) 
		{
			animator.SetInteger ("Direction", 0);
			animator.speed = .2f;
		} 
		else 
		{
			animator.speed = .8f;
		}
		if (!_dashing || _canMove == true) 
		{
			transform.Translate (flXAxis * _flSpeed * Time.deltaTime, flYAxis * _flSpeed * Time.deltaTime, 0f);

		}
		#endif
		 	 

		//________________________________________________ DEATH  ___________________________________________________________//
	

		if (_isDead == true)
		{
			if (_hasSparkled == false) 
			{
				float fadeTime = GameObject.Find ("Game Manager").GetComponent<Fading> ().BeginFade (1);	

				Destroy(Instantiate (_deathFX, transform.position, transform.rotation),5f);
				_hasSparkled = true;
				_canMove = false;

				Color invis = _mySprite.color;
				invis.a = 0f;
				_mySprite.color = invis;
			}

			_fadeTimer += Time.deltaTime;
			if (_fadeTimer >= 1f) 
			{
				SceneManager.LoadScene ("MainMenu");

				_fadeTimer = 0f;
			}
		}

	}

	void LateUpdate()
	{
		_flCurMana = Mathf.Round(_flCurMana * 100) / 100;
		_slManaBar.value = _flCurMana;

		gameObject.transform.up = Vector3.up;
	}

	public void AddHealth(float addAmount)
	{	
		_flCurHealth = _flCurHealth + addAmount;
		_slHealthBar.value = _flCurHealth;
	}

	public void TakeHealth(float takeAmount)
	{		
		_flCurHealth = _flCurHealth - takeAmount;
		_slHealthBar.value = _flCurHealth;

		//________________________________________________ DEATH __________________________________________________________//

		if (_flCurHealth <= 0f) 
		{
			PermanentManager._isGameOver = true;
			PermanentManager._flCurrentSparkles = _flSparkleValue;
			Debug.Log (PermanentManager._flCurrentSparkles);

			_isDead = true;	

		}
	}

	public void AddMana (float addAmount)
	{	
		_flCurMana = _flCurMana + addAmount;
		_slManaBar.value = _flCurMana;
	}


	public IEnumerator CastSpell(float damage)
	{
		if (_equippedSpell.GetComponent<BaseAttack> ()._flManaCost < _flCurMana) 
		{
			if (_canCastSpell == true) 
			{

				_canCastSpell = false;

				GameObject spell;
				BaseAttack spellScriptRef;
				// _PrimarySpell[0] should change appropriately to spell selected 
				spell = Instantiate (_equippedSpell, this.transform.position, this.transform.rotation) as GameObject;
				Destroy (spell, 3f);
				spellScriptRef = spell.GetComponent<BaseAttack> ();

				spellScriptRef._flDamage = damage;
				_flCurMana -= spellScriptRef._flManaCost;
				//spell.GetComponent<BaseAttack> ()._flDamage = damage;

				attackVec = attackVec.normalized;

				spell.GetComponent<Rigidbody2D> ().AddForce (attackVec * spellScriptRef._flSpeed);
				spell.transform.rotation = Quaternion.LookRotation (Vector3.forward, attackVec);
				spellScriptRef._pcOrigin = gameObject.GetComponent<PlayerController> ();


				yield return new WaitForSecondsRealtime (spellScriptRef._flAttackRate);
				_canCastSpell = true;
			}
		}



//		if (_primarySpell [_itCurSpell].GetComponent<BaseAttack> ()._flManaCost < _flCurMana) 
//		{
//			_canCastSpell = false;
//
//			GameObject spell;
//			BaseAttack spellScriptRef;
//			// _PrimarySpell[0] should change appropriately to spell selected 
//			spell = Instantiate (_primarySpell [_itCurSpell], this.transform.position, this.transform.rotation) as GameObject;
//			spellScriptRef = spell.GetComponent<BaseAttack> ();
//
//			spellScriptRef._flDamage = damage;
//			_flCurMana -= spellScriptRef._flManaCost;
//			//spell.GetComponent<BaseAttack> ()._flDamage = damage;
//
//			Vector3 v3Pos = Camera.main.WorldToScreenPoint (transform.position);
//			Vector3 v3Dir = (Input.mousePosition - v3Pos).normalized;
//
//			spell.GetComponent<Rigidbody2D> ().AddForce (v3Dir * spellScriptRef._flSpeed);
//			spell.transform.rotation = Quaternion.LookRotation (Vector3.forward, v3Dir);
//			spellScriptRef._pcOrigin = gameObject.GetComponent<PlayerController> ();
//			
//
//			yield return new WaitForSeconds (spellScriptRef._flAttackRate);
//			_canCastSpell = true;	
//		}

	}

	public IEnumerator CastSpellMouse(float damage)
	{
		if (_equippedSpell.GetComponent<BaseAttack> ()._flManaCost < _flCurMana) 
		{
			if (_canCastSpell == true) 
			{

				_canCastSpell = false;

				GameObject spell;
				BaseAttack spellScriptRef;
				// _PrimarySpell[0] should change appropriately to spell selected 
				spell = Instantiate (_equippedSpell, this.transform.position, this.transform.rotation) as GameObject;
				Destroy (spell, 3f);
				spellScriptRef = spell.GetComponent<BaseAttack> ();

				spellScriptRef._flDamage = damage;
				_flCurMana -= spellScriptRef._flManaCost;
				//spell.GetComponent<BaseAttack> ()._flDamage = damage;

				Vector3 v3Pos = Camera.main.WorldToScreenPoint (transform.position);
				Vector3 v3Dir = (Input.mousePosition - v3Pos).normalized;


				spell.GetComponent<Rigidbody2D> ().AddForce (v3Dir * spellScriptRef._flSpeed);
				spell.transform.rotation = Quaternion.LookRotation (Vector3.forward, v3Dir);
				spellScriptRef._pcOrigin = gameObject.GetComponent<PlayerController> ();


				yield return new WaitForSecondsRealtime (spellScriptRef._flAttackRate);
				_canCastSpell = true;
			}
		}
	}

	public IEnumerator WeaponAttack(float damage)
	{		

		_canWeaponAttack = false;

		Vector3 v3Pos = Camera.main.WorldToScreenPoint (transform.position);
		Vector3 v3Dir = (Input.mousePosition - v3Pos).normalized;

		GameObject attack;
		WeaponAttack weaponScriptRef;
		attack = Instantiate (_weaponAttack, this.transform.position + (v3Dir / 2f), this.transform.rotation) as GameObject;
		weaponScriptRef = attack.GetComponent<WeaponAttack> ();
		weaponScriptRef._fldmg = damage;

		attack.transform.rotation = Quaternion.LookRotation (Vector3.forward, v3Dir);

		yield return new WaitForSeconds (weaponScriptRef._flAttackRate);
		_canWeaponAttack = true;	

	}


	IEnumerator RegenMana()
	{
		while (true) 
		{
			yield return new WaitForSeconds (_flRegenSpeed);
			if (_flCurMana < _flMaxMana) 
			{
				_flCurMana += _flRegenAmount;
				if (_flCurMana >= _flMaxMana) 
				{
					_flCurMana = _flMaxMana;
				}
			}
		}
	}


}

