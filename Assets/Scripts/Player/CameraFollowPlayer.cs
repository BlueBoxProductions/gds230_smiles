﻿// Author: Darien Ross
// Contributors: 
// Purpose: 


using UnityEngine;
using System.Collections;

public class CameraFollowPlayer : MonoBehaviour {
	
    public GameObject player;

    private void Update()
    {
        Vector3 roomLoc = player.GetComponent<PlayerController>().roomLocation;
        if (roomLoc == new Vector3(0,0))
            roomLoc = new Vector3(-51, 0);
        transform.position = new Vector3(roomLoc.x, roomLoc.y, -10);
    }
}