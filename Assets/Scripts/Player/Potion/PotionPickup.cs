﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionPickup : MonoBehaviour {

	public PlayerController _playerRef;
	public float _healthReturn;
	public float _manaReturn;

	// Use this for initialization
	void Awake () 
	{
		_playerRef = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
	}


	public void OnTriggerEnter2D(Collider2D col)
	{
			if (col.tag == "Player") 
		{
			_playerRef.AddHealth (_healthReturn);
			_playerRef.AddMana (_manaReturn);

			Destroy (gameObject);
		}

	}
		

}
